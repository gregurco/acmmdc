<?php

return CMap::mergeArray(
    array(
        'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
        'name'=>'My Console Application',

        // preloading 'log' component
        'preload'=>array('log'),
        'import'=>array(
            'application.models.*',
            'application.components.*',
        ),

        // application components
        'components'=>array(
            'db'=>array(
                'class'=>'system.db.CDbConnection',
                'emulatePrepare' => true,
                'tablePrefix' => 'c_'
            ),
            'log'=>array(
                'class'=>'CLogRouter',
                'routes'=>array(
                    array(
                        'class'=>'CFileLogRoute',
                        'levels'=>'error, warning',
                    ),
                ),
            ),
        ),
        'commandMap' => array(
            'cron' => array(
                'class' => 'application.components.CronCommand',
                'dbConnection' => 'db',
            ),
        ),
    ),

    require_once(dirname(__FILE__).'/config.php')
);