<?php

class m140828_190119_init extends CDbMigration
{
	public function up()
	{
        $this->createTable('c_processing', array(
            'id' => 'pk',
            'compiler' => 'string',
            'file_text' => 'text',
            'result' => 'int NOT NULL DEFAULT 0',
            'tests' => 'text',
            'log_compile' => 'text',
            'status' => 'int NOT NULL DEFAULT 1',
            'p_id' => 'int',
            'limit_time' => 'double unsigned',
            'limit_memory' => 'double unsigned'
        ), "DEFAULT CHARSET=utf8");
	}

	public function down()
	{
        $this->dropTable('c_processing');
	}
}